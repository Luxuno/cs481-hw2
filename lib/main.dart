import 'package:flutter/material.dart';
import 'package:simple_application_v2/custom_icons_icons.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('The next generation of gaming is here', style: TextStyle(
                      fontWeight: FontWeight.bold),
                  ),
                ),
                Text('Which one will you decide?', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    Widget buttonSelection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.blue, CustomIcons.playstation_logo, 'Playstation Line-up'),
          _buildButtonColumn(Colors.green, CustomIcons.xbox_one_logo, 'XBOX Line-up'),
          _buildButtonColumn(Colors.black, Icons.attach_money, 'Console Availability'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'With the end of 2020 drawing year, two major gaming consoles are making the leap to the next generation. '
        'Sony\'s Playstation 5 and Microsoft\'s XBOX Series X/S both boast their technical prowess allowing for '
        'faster load times, higher frame rates, and higher resolutions. Both consoles plan to release in November 2020 with '
        'the XBOX Series X/S releasing on November 10th and the Playstation 5 releasing on November 12th with both being priced at \$499.99',
        softWrap: true,
      ),
    );



    return MaterialApp(
      title: 'Next Gen News',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Next Generation Gaming'),
          backgroundColor: Colors.pinkAccent,
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/NEXTGEN.png',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSelection,
            textSection,
          ],
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}
